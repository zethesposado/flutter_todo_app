import 'package:freezed_annotation/freezed_annotation.dart';

part 'task.freezed.dart';
part 'task.g.dart';

@freezed
class Task with _$Task {
    const factory Task({
        required int id,
        required int userId,
        required String description,
        required String? imageLocation,
        required int isDone
    }) = _Task;

    factory Task.fromJson(Map<String, dynamic> json) => _$TaskFromJson(json);
}

/* class Task {
    late final int id;
    late final int userId;
    late final String description;
    late final String? imageLocation;
    late final int isDone;

    Task({ 
        required this.id, 
        required this.userId,
        required this.description,
        required this.imageLocation,
        required this.isDone
    });

    factory Task.fromJson(Map<String, dynamic> json) {
        return Task(
            id: json['id'],
            userId: json['userId'],
            description: json['description'],
            imageLocation: json['imageLocation'],
            isDone: json['isDone']
        );
    }
} */