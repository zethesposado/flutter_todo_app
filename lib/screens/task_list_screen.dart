import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/models/task.dart';
import '/providers/user_provider.dart';
import '/utils/api.dart';
import '/utils/functions.dart';
import '/widgets/add_task_dialog.dart';
import '/widgets/task_item_tile.dart';

class TaskListScreen extends StatefulWidget {
    @override
    TaskListScreenState createState() => TaskListScreenState();
}

class TaskListScreenState extends State<TaskListScreen> {

    Future<List<Task>>? _futureTasks;

    void getTasks() {
        final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;
        setState(() {
            _futureTasks = API(accessToken).getTasks().catchError((error) {
                showSnackBar(context, error.message);
            });
        });
    }

    void showAddTaskDialog(BuildContext context) {
        showDialog(
            context: context, 
            builder: (BuildContext context) => AddTaskDialog()
        ).then((value) {
            getTasks();
        });
    }

    void logout() async {
        Provider.of<UserProvider>(context, listen: false).setAccessToken(null);
        Provider.of<UserProvider>(context, listen: false).setUserId(null);

        SharedPreferences prefs = await SharedPreferences.getInstance();

        prefs.remove('accessToken');
        prefs.remove('userId');

        Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false);
        showSnackBar(context, 'Logout succesfully');
    }

    Widget showTasks(List? tasks) {
        var cltTasks = tasks!.map((task) => TaskItemTile(task)).toList();
        
        return ListView(
            children: cltTasks
        );
    }

    @override
    void initState() {
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) { 
            getTasks();
        });
    }

    @override
    Widget build(BuildContext context) {
        Widget taskListView = FutureBuilder(
            future: _futureTasks,
            builder: (context, snapshot) {
                if (snapshot.hasData) {
                    return showTasks(snapshot.data as List);
                }
                return Center(
                    child: CircularProgressIndicator(),
                );
            }
        );

        return Scaffold(
            appBar: AppBar(title: Text('Todo List')),
            drawer: Drawer(
                child: ListView(
                    children: [
                        ListTile(
                            title: Text('Logout'),
                            onTap: () async {
                                logout();
                            }
                        )
                    ]
                )
            ),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16),
                child: taskListView
            ),
            floatingActionButton: FloatingActionButton(
                child: Icon(Icons.add),
                backgroundColor: Color.fromRGBO(225, 212, 7, 1),
                foregroundColor: Colors.black,
                onPressed: () {
                    showAddTaskDialog(context);
                }
            )
        );
    }
}